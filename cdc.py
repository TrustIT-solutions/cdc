#!/usr/bin/python
# -*- coding: utf-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

"Módulo para utilizar el servicio web Constatación de Comprobantes de AFIP"


class CDC(object):

    def __init__(self):
        self.resultado = None

    def ConstatarComprobante(self, cbte_modo, cuit_emisor, pto_vta, cbte_tipo,
                             cbte_nro, cbte_fch, imp_total, cod_autorizacion,
                             doc_tipo_receptor=None, doc_nro_receptor=None,
                             **kwargs):
        """Método de Constatación de Comprobantes"""

        response = self.client.ComprobanteConstatar(
                    Auth={'Token': self.Token,
                          'Sign': self.Sign,
                          'Cuit': self.Cuit},
                    CmpReq={
                        'CbteModo': cbte_modo,
                        'CuitEmisor': cuit_emisor,
                        'PtoVta': pto_vta,
                        'CbteTipo': cbte_tipo,
                        'CbteNro': cbte_nro,
                        'CbteFch': cbte_fch,
                        'ImpTotal': imp_total,
                        'CodAutorizacion': cod_autorizacion,
                        'DocTipoReceptor': doc_tipo_receptor,
                        'DocNroReceptor': doc_nro_receptor,
                        }
                    )
        result = response['ComprobanteConstatarResult']
        self.__analizar_errores(result)
        if 'CmpResp' in result:
            resp = result['CmpResp']
            self.Resultado = result['Resultado']
            self.FchProceso = result.get('FchProceso', "")
            self.observaciones = []
            for obs in result.get('Observaciones', []):
                self.Observaciones.append("%(Code)s: %(Msg)s" % (obs['Obs']))
                self.observaciones.append({
                    'code': obs['Obs']['Code'],
                    'msg': obs['Obs']['Msg'].replace("\n", "")
                                                    .replace("\r", "")})
            self.Obs = '\n'.join(self.Observaciones)
            self.FechaCbte = resp.get('CbteFch', "") #.strftime("%Y/%m/%d")
            self.CbteNro = resp.get('CbteNro', 0) # 1L
            self.PuntoVenta = resp.get('PtoVta', 0) # 4000
            self.ImpTotal = str(resp['ImpTotal'])
            self.EmisionTipo = resp['CbteModo']
            self.DocTipo = resp.get('DocTipoReceptor', '')
            self.DocNro = resp.get('DocNroReceptor', '')
            cod_aut = str(resp.get('CodAutorizacion', "")) # 60423794871430L
            if self.EmisionTipo == 'CAE':
                self.CAE = cod_aut
            elif self.EmisionTipo == 'CAEA':
                self.CAEA = cod_aut
            elif self.EmisionTipo == 'CAI':
                self.CAI = cod_aut
        return True

    # def ConsultarModalidadComprobantes(self, sep="|"):
    #     "Recuperador de modalidades de autorización de comprobantes"
    #     response = self.client.ComprobantesModalidadConsultar(
    #                 Auth={'Token': self.Token, 'Sign': self.Sign, 'Cuit': self.Cuit},
    #                 )
    #     result = response['ComprobantesModalidadConsultarResult']
    #     self.__analizar_errores(result)
    #     return [(u"\t%(Cod)s\t%(Desc)s\t" % p['FacModTipo']).replace("\t", sep)
    #              for p in result['ResultGet']]

    # def ConsultarTipoComprobantes(self, sep="|"):
    #     "Recuperador de valores referenciales de códigos de Tipos de comprobante"
    #     response = self.client.ComprobantesTipoConsultar(
    #                 Auth={'Token': self.Token, 'Sign': self.Sign, 'Cuit': self.Cuit},
    #                 )
    #     result = response['ComprobantesTipoConsultarResult']
    #     self.__analizar_errores(result)
    #     return [(u"\t%(Id)s\t%(Desc)s\t" % p['CbteTipo']).replace("\t", sep)
    #              for p in result['ResultGet']]

    # def ConsultarTipoDocumentos(self, sep="|"):
    #     "Recuperador de valores referenciales de códigos de Tipos de Documentos"
    #     response = self.client.DocumentosTipoConsultar(
    #                 Auth={'Token': self.Token, 'Sign': self.Sign, 'Cuit': self.Cuit},
    #                 )
    #     result = response['DocumentosTipoConsultarResult']
    #     self.__analizar_errores(result)
    #     return [(u"\t%(Id)s\t%(Desc)s\t" % p['DocTipo']).replace("\t", sep)
    #              for p in result['ResultGet']]

    # def ConsultarTipoOpcionales(self, sep="|"):
    #     "Recuperador de valores referenciales de códigos de Tipos de datos Opcionales"
    #     response = self.client.OpcionalesTipoConsultar(
    #                 Auth={'Token': self.Token, 'Sign': self.Sign, 'Cuit': self.Cuit},
    #                 )
    #     result = response['OpcionalesTipoConsultarResult']
    #     res = result['ResultGet'] if 'ResultGet' in result else []
    #     self.__analizar_errores(result)
    #     return [(u"\t%(Id)s\t%(Desc)s\t" % p['OpcionalTipo']).replace("\t", sep)
    #              for p in res]
